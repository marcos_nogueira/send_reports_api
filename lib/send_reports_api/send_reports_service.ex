defmodule SendReportsApi.SendReportsService do

  def send_report(params) do
    with {:ok, %{status_code: 200}} <- HTTPoison.post(
      "https://api.mailgun.net/v3/sandbox4938af71925644cd8f9711cef31eac3b.mailgun.org/messages", 
      {:multipart, [
        {"from","cadprod@sandbox4938af71925644cd8f9711cef31eac3b.mailgun.org"},
        {"to", params["to"]},
        {"cc", "marcos.nogueira@skyhub.com.br"},
        {"text", "Segue Relatorio"},
        {"subject", params["subject"]},
        {"attachment", params["attachment"],
        {"form-data", [
          {"name", "\"attachment\""},
          {"filename", params["file_name"]}]},
          [{"Content-Type", "text/csv"}]}]},
          [{"Authorization", 
          "Basic YXBpOjI1YzcyMDBiYThiMjY1Y2EyNmNlOThiOWVhMjg0NDkxLWQ1ZTY5YjBiLTAzZmZkNjg5"}]) do
      {:ok, "Email Enfileirado para envio"}
    else 
      {:ok, %{status_code: 400}} ->
        {:error, "Algo esta erro"}          
      
    end         
  end

end