defmodule SendReportsApiWeb.Router do
  use SendReportsApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", SendReportsApiWeb do
    pipe_through :api

    get "/", ReportController, :index
    post "/report", ReportController, :create
  end
end
