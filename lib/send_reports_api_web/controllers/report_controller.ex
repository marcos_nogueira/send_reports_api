defmodule SendReportsApiWeb.ReportController do
  use SendReportsApiWeb, :controller

  alias SendReportsApi.SendReportsService

  def index(conn, _params) do
    json(conn, %{ok: "Send_Reports"})
  end

  def create(conn, params) do
    with {:ok, "Email Enfileirado para envio"} <- SendReportsService.send_report(params) do
      json(conn, %{ok: "Email Enfileirado para Envio com Sucesso"})
    else
      {:error, _} ->
        json(conn, %{error: "Tente novamente"})  
    end
  end

end

