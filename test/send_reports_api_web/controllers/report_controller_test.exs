defmodule SendReportsApiWeb.ReportControllerTest do
  use SendReportsApiWeb.ConnCase, async: false


  @valid_attrs %{
    "attachment" => "00001;Primeiro produto;321.0;20\n00002;Produto Dois;123.0;36\n",
    "file_name" => "products_report.csv",
    "subject" => "Relatorio de Produtos",
    "to" => "marcos.nogueira@skyhub.com.br"}

  @invalid_attrs %{
    "attachment" => "00001;Primeiro produto;321.0;20\n00002;Produto Dois;123.0;36\n",
    "file_name" => "products_report.csv",
    "subject" => "Relatorio de Produtos",
    "to" => "marcos"}

  test "shold response 200 in /api get", %{conn: conn} do
    with {:ok, conn} <- get conn, "/api" do
      assert json_response(conn, 200)  
    end
  end               

  test "shold return status_code 200 if valid attributes" do
    post build_conn(), "/api/report", @valid_attrs 
    assert {:ok, %{status_code: 200}}
  end

  test "shold return status_code 400 with invalid attributes" do
    post build_conn(), "/api/report", @invalid_attrs
    assert {:ok, %{status_code: 400}}
  end
end


